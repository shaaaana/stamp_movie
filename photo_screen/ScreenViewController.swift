//
//  ScreenViewController.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/17.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit
import AVFoundation

class ScreenViewController: UIViewController {
    @IBOutlet var imgview: UIImageView!
    var movieArr: [NSDictionary] = []
    var imgArr: [String] = []
    @IBOutlet var anilabel : UILabel!
    @IBOutlet weak var WataSlider: UISlider!
    var countNum = 0
    var timer: NSTimer!
    var myLabel: UILabel!
    var player:AVAudioPlayer?
    var soundManager = SEManager() //SEManagerでインスタンス化した変数
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // UIImage インスタンスの生成
        imgArr.append("character.png")
        imgArr.append("2.jpg")
        imgArr.append("bike.png")
        self.firstImg()
    }
    
    
    
    func firstImg(){
        let image1:UIImage? = UIImage(named:imgArr[0])
        
        imgview.image = image1
        
        // 画像の中心を設定
        imgview.center = CGPointMake(187.5, 233.5)
        
        // UIImageViewのインスタンスをビューに追加
        self.view.addSubview(imgview)
    }

    
    func update() {
        self.animate()
        anilabel.text = String(countNum)
        countNum++
        changeImg()
    }
    
    
    func animate(){
        //変形を設定するCGAffineTransformの変数
        var transform:CGAffineTransform = CGAffineTransformIdentity
        //アニメーションの所要時間を持つ変数
        let duration:Double = 0.5

        transform = CGAffineTransformMakeTranslation(0, -20)
        UIView.animateWithDuration(3.0, animations: {() -> Void in
            self.anilabel.center = CGPoint(x: 100,y: 300);
            }, completion: {(Bool) -> Void in
                self.anilabel.text = "move";
                UIView.animateWithDuration(duration, animations: {() -> Void in
                    self.anilabel.transform = CGAffineTransformIdentity
                    })
                    { (Bool) -> Void in
                }
        })
    }
    
    
    func changeImg(){
//        let view: UIImageView = UIImageView()
//        view.removeFromSuperview()
        var roop = (imgArr.count)
        print(imgArr.count)
        var number = countNum%roop
        print(number)
        imgview.removeFromSuperview()
        let image:UIImage? = UIImage(named:imgArr[number])
//        imaview = UIImageView(image:image)
        imgview.image = image
        self.view.addSubview(imgview)
        
//        myLabel.layer.position = CGPointMake(-30, -30)
        
        // アニメーション処理
        UIView.animateWithDuration(NSTimeInterval(CGFloat(3.0)),
            animations: {() -> Void in
                
                // 移動先の座標を指定する.
//                self.myLabel.center = CGPoint(x: self.view.frame.width/2,y: self.view.frame.height/2);
                
            }, completion: {(Bool) -> Void in
        })

    }
    
    
    // 日時を取得する
    func getDate()->NSString {
        let now = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") // ロケールの設定
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss" // 日付フォーマットの設定
        var thisDate = dateFormatter.stringFromDate(now)
        return thisDate
    }
    
    
        // スライダーを動かした時にこのメソッドを毎回呼び出す
        @IBAction func updateValue(sender: UISlider) {
            // スライダーの位置を取得する
                change()
        }
    
    
    func change(){ //練習用
//            var level = String(Int(self.WataSlider.value * 10000 / 100))
//            let image1:UIImage? = UIImage(named:"2.jpg")
//            let imageView = UIImageView(image:image1)
//            self.view.addSubview(imageView)
            //timerを破棄する.
            timer.invalidate()
    }
    //
    
    
    @IBAction func playStart(){
//        self.firstImg()
//        print("sound:\(SEManager.Sound())")
        
        //クラスメソッドから、AppDelegateに保存したファイル名を直接呼び出す。
        soundManager.sePlay(SEManager.Sound())
//        if((timer) == nil){
            self.settimer()
//        }
        
    }
    
    func settimer(){
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
    }
    
    
    @IBAction func playPause(){
        timer.invalidate()
        soundManager.pause()
    }
    
    
    func Animation(){
        //変形を設定するCGAffineTransformの変数
        var transform:CGAffineTransform = CGAffineTransformIdentity
        //アニメーションの所要時間を持つ変数
        let duration:Double = 0.5
        //上に移動
        transform = CGAffineTransformMakeTranslation(0, -20)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

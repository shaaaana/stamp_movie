//
//  drawView.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/20.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit
import CoreGraphics

class DrawView: UIView {
        
        //6.
    var lines: [Line] = []
    var lastPoint: CGPoint!
    var color: String = "red"
    var width: Int = 10
    
    var r: CGFloat!
    var g: CGFloat!
    var b: CGFloat!
    var a: CGFloat!
    
    
    enum Width: Int {
        case large = 10
        case medium = 6
        case small = 3
    }
    
    
    enum Color: String {
        case red = "red"
        case black = "black"
        case blue = "blue"
    }
    
    
        //4.初期化
    required init(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)!
            //self.backgroundColor = UIColor.blackColor() //チェック用
            self.r = 1
            self.g = 0
            self.b = 0
            self.a = 1
    }
        
        //7.
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //画面上のタッチ情報を取得
        let touch = touches.first!
        //画面上でドラッグしたx座標の移動距離
        lastPoint = touch.locationInView(self)
    }
        
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //画面上のタッチ情報を取得
        let touch = touches.first!
        let newPoint = touch.locationInView(self)
        lines.append(Line(start: lastPoint, end: newPoint))
        lastPoint = newPoint
        self.setNeedsDisplay()
    }
    
    //8.
    override func drawRect(rect: CGRect) {
        var context = UIGraphicsGetCurrentContext()
        CGContextBeginPath(context)
        
        for line in lines {
            CGContextMoveToPoint(context, line.start.x, line.start.y)
            CGContextAddLineToPoint(context, line.end.x, line.end.y)
        }
        
        print("r=\(self.r)")
        print("b=\(self.b)")
        CGContextSetRGBStrokeColor(context, self.r, self.g, self.b, self.a) //線の色
        CGContextSetLineWidth(context, 10)  //線の太さ
        CGContextSetLineCap(context, CGLineCap.Round)   //線を滑らかに
        CGContextStrokePath(context)
    }
    
    
    
    @IBAction func set_draw(sender: AnyObject){
        
    }

}

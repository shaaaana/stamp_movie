//
//  SEManager.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/18.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation // AVFoundationフレームワークをインポートする


class SEManager: NSObject {
    
    //player変数をAVAudioPlayerのインスタンス化
    
    var player:AVAudioPlayer?
    
    var sound:String = ""
    
    
    //音を再生するsePlayメソッド
    func sePlay(soundName: String)->Bool{
        print(soundName)
        //String型の引数からサウンドファイルを読み込む
        let url = NSBundle.mainBundle().bundleURL.URLByAppendingPathComponent(soundName)
        do {
            try player = AVAudioPlayer(contentsOfURL: url)
            player!.prepareToPlay()	            	//音声を即時再生させる
            
            player!.play()                                	//音を再生する
            
        }catch let error as NSError{
            print("Error!")
            print (error)
            return false
        }
        return true
    }
    
    func pause(){
         player?.pause()
    }
    
    func store(){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.sound = self.sound //appDelegateの変数を操作
    }
    
    class func Sound()->String{
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        return appDelegate.sound
    }
    
}
//
//  JsonEncodeViewController.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/18.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit

class JsonEncodeViewController: UIViewController{
    
    var json:NSData!
    var myTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.backgroundColor = UIColor.lightGrayColor()
        
        // 結果表示用のTextViewを用意.
        myTextView = UITextView(frame: CGRectMake(10, 50, self.view.frame.width - 20, 500))
        
        myTextView.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 1, alpha: 1.0)
        myTextView.layer.masksToBounds = true
        myTextView.layer.cornerRadius = 20.0
        myTextView.layer.borderWidth = 1
        myTextView.layer.borderColor = UIColor.blackColor().CGColor
        myTextView.font = UIFont.systemFontOfSize(CGFloat(20))
        myTextView.textColor = UIColor.blackColor()
        myTextView.textAlignment = NSTextAlignment.Left
        myTextView.dataDetectorTypes = UIDataDetectorTypes.All
        myTextView.layer.shadowOpacity = 0.5
        myTextView.layer.masksToBounds = false
        myTextView.editable = false
        
        self.view.addSubview(myTextView)
        
        // dictionaryで送信するJSONデータを生成.
        
        var myDict:NSMutableDictionary = NSMutableDictionary()
        myDict.setObject("object1", forKey: "key1")
        myDict.setObject("object2", forKey: "key2")
        myDict.setObject("object3", forKey: "key3")
        myDict.setObject("object4", forKey: "key4")
        
        // 作成したdictionaryがJSONに変換可能かチェック.
        if NSJSONSerialization.isValidJSONObject(myDict){
            
            // DictionaryからJSON(NSData)へ変換.
            do {
               json =  try NSJSONSerialization.dataWithJSONObject(myDict, options: NSJSONWritingOptions.PrettyPrinted)
            } catch let error {
                // エラーが起こったらここに来るのでエラー処理などをする
                print("Json変換失敗")
                print(error)
            }
            
            // 生成したJSONデータの確認.
            print(NSString(data: json, encoding: NSUTF8StringEncoding)!)
            
        }
        
        // Http通信のリクエスト生成.
        let myCofig:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        let myUrl:NSURL = NSURL(string: "http://localhost:8888/json_decode.php")!
        
        let myRequest:NSMutableURLRequest = NSMutableURLRequest(URL: myUrl)
        
        let mySession:NSURLSession = NSURLSession(configuration: myCofig)
        
        myRequest.HTTPMethod = "POST"
        
        // jsonのデータを一度文字列にして、キーと合わせる.
        let myData:NSString = "json=\(NSString(data: json, encoding: NSUTF8StringEncoding)!)"
        
        // jsonデータのセット.
        myRequest.HTTPBody = myData.dataUsingEncoding(NSUTF8StringEncoding)
        
        let myTask:NSURLSessionDataTask = mySession.dataTaskWithRequest(myRequest, completionHandler: { (data, response, err) -> Void in
            
            // バックグラウンドだとUIの処理が出来ないので、メインスレッドでUIの処理を行わせる.
            dispatch_async(dispatch_get_main_queue(), {
                self.myTextView.text = NSString(data: data!, encoding: NSUTF8StringEncoding)! as String
            })
        })
        
        myTask.resume()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}

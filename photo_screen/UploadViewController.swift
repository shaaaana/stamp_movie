//
//  UploadViewController.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/18.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit

class UploadViewController: UIViewController,NSURLSessionTaskDelegate{
    var json:NSData!
    var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var imgName:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var imgName = "2.jpg"
        
    
        // 通信のリクエスト生成.
        let myCofig:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        let myUrl:NSURL = NSURL(string: "http://localhost:8888/Images/upload.php")!
        
        let myRequest:NSMutableURLRequest = NSMutableURLRequest(URL: myUrl)
        myRequest.HTTPMethod = "POST"
        
        let mySession:NSURLSession = NSURLSession(configuration: myCofig, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        
        // 画像データを読み出し、Data型に変換する.
        var myfile:NSData = UIImageJPEGRepresentation(UIImage(named: imgName)!, 0.5)!
        var myImage:UIImage = UIImage(data: myfile)!
        
        // 送信するファイルのプレビュー.
        let myImageView: UIImageView = UIImageView(frame: CGRectMake(0,0,100,120))
        myImageView.image = myImage
        myImageView.layer.position = CGPoint(x: self.view.bounds.width/2, y: 200.0)
        
        self.view.addSubview(myImageView)
        
        // アップロード用のタスクを生成.
        var myTask:NSURLSessionUploadTask = mySession.uploadTaskWithRequest(myRequest, fromData: myfile)
        // タスクの実行.
        myTask.resume()
    }
    
    /*
    通信終了時に呼び出されるデリゲート.
    */
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        print("didCompleteWithError")
        // エラーが有る場合にはエラーのコードを取得.
        print(error?.code)
    }
    
    func URLSession(session: NSURLSession,
        task: NSURLSessionTask,
        didReceiveChallenge challenge: NSURLAuthenticationChallenge,
        completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential?)
        -> Void) {
        
        print("willPerformHTTPRedirection")
        
    }
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        
        print("didSendBodyData")
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}
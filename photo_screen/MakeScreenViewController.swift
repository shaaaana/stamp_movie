//
//  MakeScreenViewController.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/18.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit


class MakeScreenViewController: UIViewController {
    var soundManager = SEManager() //SEManagerでインスタンス化した変数
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnTapped(sender: UIButton){
        //サウンドファイル名を格納するsound変数。初期値に空の文字を入れておく
        
        switch sender.tag{
            
        case 1:
            
            print("設定\(sender.tag)")
            
            soundManager.sound = "bgm1.mp3"
            print("soundManager:\(soundManager.sound)")
            
            
        case 2:
            
            print("設定\(sender.tag)")
            
            soundManager.sound = "bgm2.mp3"
            
                
        default:
            
            print("どのボタンでもありません")
            
        }
        
        soundManager.store()
        
        //引数にsound変数を設定して、SEManagerクラスのsePlayメソッドを呼び出す
        
//        soundManager.sePlay(sound)
        
    }
    
}

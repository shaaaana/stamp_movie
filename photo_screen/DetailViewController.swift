//
//  DetailViewController.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/24.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit
import Alamofire

class DetailViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet var webview : UIWebView? = nil
    private var myTextField: UITextField!
    
    var info: String?
    var view_url: String = ""
    var phototitle: String = ""
    var com_text:String = ""
    var json:NSData!
    var id:Int = 0
    
    @IBOutlet var myTextView: UITextView!
    
    @IBOutlet weak var titleLabel: UILabel!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // UITextFieldを作成する.
        print(id)
        myTextField = UITextField(frame: CGRectMake(0,0,200,30))
        
        // 表示する文字を代入する.
        myTextField.text = ""
        
        // Delegateを設定する.
        myTextField.delegate = self
        
        // 枠を表示する.
        myTextField.borderStyle = UITextBorderStyle.RoundedRect
        
        // UITextFieldの表示する位置を設定する.
        myTextField.layer.position = CGPoint(x:self.view.bounds.width/2,y:460);
        
        // Viewに追加する.
        self.view.addSubview(myTextField)
        loadAddressURL()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadAddressURL() {
        titleLabel.text = phototitle
        let requestURL = NSURL(string: view_url)
        let req = NSURLRequest(URL: requestURL!)
        webview!.loadRequest(req)
    }
    
    
    
    func displayAlert(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func CommentTouched(){
        if self.com_text == ""{
            
            // 入力が空欄の時のバリデーション
            displayAlert("エラー", message: "にゅうりょくしてね!")
            return
        }

        print(self.com_text)
        save()
    }
    
    
    func save(){
        let params: [String: AnyObject] = [
            "image_id": self.id,
            "content": self.com_text,
            "password": "sadffkafjldj"
        ]
        
        // HTTP通信
        Alamofire.request(.POST, "\(Env.DocumentRoot())/api/comments", parameters: params, encoding: .URL).responseJSON { (response) in
            
            guard let data = response.result.value else{
                print(response.result.value)
                print("Request failed with error")
                return
            }
            print(data)
            print("=============JSON================")
        }
    }
    
    
    //    UITextFieldが編集された直後に呼ばれるデリゲートメソッド.
    //    */
    func textFieldDidBeginEditing(textField: UITextField){
        //        print("textFieldDidBeginEditing:" + textField.text!)
        print(textField)
        self.com_text = textField.text!
    }
    
    /*
    UITextFieldが編集終了する直前に呼ばれるデリゲートメソッド.
    */
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        print("textFieldShouldEndEditing:" + textField.text!)
        self.com_text = textField.text!
        return true
    }
    
    /*
    改行ボタンが押された際に呼ばれるデリゲートメソッド.
    */
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.com_text = textField.text!
        return true
    }
}

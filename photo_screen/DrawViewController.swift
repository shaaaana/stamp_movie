//1.クリアボタンを設置→viewController.swiftにつなげる
//2.新規ファイル→CocoaTouch classのファイル作成（drawView.swift)
//3.Viewを設置→custom classにdrawViewを入力してつなげる
//5.swiftファイルを作成(line.swift)


import UIKit

class DrawViewController: UIViewController {
    
    //9.描画エリアを接続
    @IBOutlet weak var drawViewArea: DrawView!
    
    enum BtnTag: Int {
        case Zero = 1, Red, Black, Blue
        static let allValues: [BtnTag] = [ Red, Black, Blue]
        
        func toStr() -> String {
//            if self == Other {
//                return "other"
//            }else{
                return String(self )
//            }
        }
        
        func set_color(btn: UIButton)->Void{
            switch self{
             case .Red:
                btn.backgroundColor = UIColor.redColor()
             case .Black:
                btn.backgroundColor = UIColor.blackColor()
             case .Blue:
                btn.backgroundColor = UIColor.blueColor()
             default: break
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        for btag in BtnTag.allValues {
            var x = CGFloat(35 * btag.rawValue - 1)
            var btn = UIButton(frame: CGRectMake(x, 480, 30, 20))
            btn.tag = btag.rawValue
            switch(btag){
                case .Red:
                    btn.backgroundColor = UIColor.redColor()
                case .Black:
                    btn.backgroundColor = UIColor.blackColor()
                case .Blue:
                    btn.backgroundColor = UIColor.blueColor()
                default: break
            }
//            BtnTag.set_color(btn)
            
//            btn.setTitle(btag.toStr(), forState: .Normal)
            btn.addTarget(self, action: "pushed:", forControlEvents: .TouchUpInside)
            self.view.addSubview(btn)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func claerTapped(sender: UIButton) {      //1.
        //10.
        let theDrawView : DrawView = drawViewArea as DrawView    //
        theDrawView.lines = []
        theDrawView.setNeedsDisplay()
    }
    
    
    func pushed(sender: UIButton){
        var btag = BtnTag(rawValue: sender.tag)!
//        let theDrawView : DrawView = drawViewArea as DrawView
        if btag == .Blue {
            drawViewArea.r = 0
            drawViewArea.g = 0
            drawViewArea.b = 1
            drawViewArea.a = 1
            print("Blue Button pushed!")
        }else if btag == .Red{
            drawViewArea.r = 1
            drawViewArea.g = 0
            drawViewArea.b = 0
            drawViewArea.a = 1
            print("Red Number is " + btag.toStr())
        }else{
            print("Other Number is " + btag.toStr())
            drawViewArea.r = 0
            drawViewArea.g = 0
            drawViewArea.b = 255
            drawViewArea.a = 1
        }
    }
    
    
    @IBAction func set_draw(sender: AnyObject){
        drawViewArea.color = "red"
    }
    
}
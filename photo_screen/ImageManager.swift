
//
//  ImageManager.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/26.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit

class ImageManager: NSObject {
    
    let myDict:NSMutableDictionary = NSMutableDictionary()
    var myRequest:NSMutableURLRequest!
    var myData:NSString!
    var mySession:NSURLSession!
    var json:NSData!
    var title: String!
    var name: String!
    var password: String!
    
    override init() {
        myDict.setObject("object1", forKey: "image")
        myDict.setObject("object2", forKey: "name")
        myDict.setObject("object3", forKey: "image_url")
        myDict.setObject("object4", forKey: "title")
    }

    func postReqest(){
        
        // 作成したdictionaryがJSONに変換可能かチェック.
        if NSJSONSerialization.isValidJSONObject(myDict){
            
            // DictionaryからJSON(NSData)へ変換.
            do {
                let json =  try NSJSONSerialization.dataWithJSONObject(myDict, options: NSJSONWritingOptions.PrettyPrinted)
                self.json = json
                // 生成したJSONデータの確認.
                print(NSString(data: json, encoding: NSUTF8StringEncoding)!)
                
            } catch let error {
                // エラーが起こったらここに来るのでエラー処理などをする
                print("Json変換失敗")
                print(error)
            }
            
        }
        
        // Http通信のリクエスト生成.
        let myCofig:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        let myUrl:NSURL = NSURL(string: "http://localhost:3000/images")!
        
        self.myRequest = NSMutableURLRequest(URL: myUrl)
        
        self.mySession = NSURLSession(configuration: myCofig)
        print(self.mySession)
        
        myRequest.HTTPMethod = "POST"
        
        //        myRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        // jsonのデータを一度文字列にして、キーと合わせる.
        self.myData = "json=\(NSString(data: json, encoding: NSUTF8StringEncoding)!)"
        
        // jsonデータのセット.
        myRequest.HTTPBody = myData.dataUsingEncoding(NSUTF8StringEncoding)
        
    }
    
    
    func getRequest(){
        // 非同期でAPIを叩いている
        // 通信用のConfigを生成.
//        let myConfig:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
//        
//        // Sessionを生成.
//        let mySession:NSURLSession = NSURLSession(configuration: myConfig, delegate: self, delegateQueue: nil)
//        
//        // 通信先のURLを生成.
//        let myUrl:NSURL = NSURL(string: "http://localhost:3000/images.json")!
//        
//        // タスクの生成.
//        let myTask:NSURLSessionDataTask = mySession.dataTaskWithURL(myUrl, completionHandler: { (data, response, err) -> Void in
//            
//            // 受け取ったjsonデータを表示.
//            dispatch_async(dispatch_get_main_queue(), {
//                //                self.myTextView.text = NSString(data: data!, encoding: NSUTF8StringEncoding)! as String
//            })
//            
//            // 受け取ったJSONデータをパースする.
//            do{
//                var jsonarr:NSArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSArray
//                
//                // バックグラウンドだとUIの処理が出来ないので、メインスレッドでUIの処理を行わせる.
//                dispatch_async(dispatch_get_main_queue(), {
//                    
//                    // パースしたJSONデータへのアクセス.
//                    for dic in jsonarr as! [NSDictionary]{
//                        //                        for value in dic{
//                        //                            var info = "\(self.myTextView.text)\n\(value.key) : \(value.value)"
//                        //                            self.cellItems.addObject(info)
//                        //                        }
//                        print("dic:\(dic)")
//                        //                            self.cellItems.addObject(dic)
//                        
//                    }
//                })
//            }catch{
//                
//            }
//        })
//        // タスクの実行.
//        myTask.resume()
        //        }
    }
}

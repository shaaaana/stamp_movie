//
//  MyStampViewController.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/19.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit

class MakeStampViewController: UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,NSURLSessionTaskDelegate{
    var json:NSData!
    var imgName:String = ""
    // 「ud」というインスタンスをつくる。
    let ud = NSUserDefaults.standardUserDefaults()
    
    var pickerController = UIImagePickerController()
    
    var photo_num: Int = 0
    //スタンプ画像を配置するUIView
    @IBOutlet var canvasView: UIView!
    //AppDelegateを使うための変数
    var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    //UIImagePickerControllerで取得した画像を表示
    @IBOutlet var mainImageView:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //UIImagePickerControllerのデリゲートメソッドを使用する設定
        pickerController.delegate = self
        
        if((ud.objectForKey("photo_num") == nil)){
            ud.setObject(0, forKey: "photo_num")
        }else{
            print(ud.objectForKey("photo_num"))
            self.photo_num = ud.objectForKey("photo_num") as! Int
            self.photo_num += 1
            ud.setObject(photo_num, forKey: "photo_num")
        }
    }
    
    
    func send_Img(){
        imgName = "#\(self.photo_num).png"
        print("imgName:\(imgName)")
        
        // 通信のリクエスト生成.
        let myCofig:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        let myUrl:NSURL = NSURL(string: "http://localhost:8888/Images/upload.php")!
        
        let myRequest:NSMutableURLRequest = NSMutableURLRequest(URL: myUrl)
        myRequest.HTTPMethod = "POST"
        
        let mySession:NSURLSession = NSURLSession(configuration: myCofig, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        
        // 画像データを読み出し、Data型に変換する.
//        var myfile:NSData = UIImageJPEGRepresentation(UIImage(named: imgName)!, 0.5)!
//        var myImage:UIImage = UIImage(data: myfile)!
        let imageData: NSData = UIImagePNGRepresentation(mainImageView.image!)!
        
        
        // 送信するファイルのプレビュー.
        let myImageView: UIImageView = UIImageView(frame: CGRectMake(0,0,100,120))
        myImageView.image = mainImageView.image
        myImageView.layer.position = CGPoint(x: self.view.bounds.width/2, y: 200.0)
        self.view.addSubview(myImageView)
        
        // 送信するデータを生成・リクエストにセット.
        let str:NSString = imgName
        let mystrData:NSData = str.dataUsingEncoding(NSUTF8StringEncoding)!
        myRequest.HTTPBody = mystrData
        
        //送信するパラメータを設定する.rubyであればparams[:text]がstrになる
//        let params = ["text": str, "file": NetData(pngImage: img, filename: "logo")]
        // アップロード用のタスクを生成.
        
        var myTask:NSURLSessionUploadTask = mySession.uploadTaskWithRequest(myRequest, fromData: imageData)
        // タスクの実行.
        myTask.resume()
    }
    
    
    //画面表示の直前に呼ばれるメソッド
    
    override func viewWillAppear(animated: Bool) {
        //viewWillAppearを上書きするときに必要な処理
        super.viewWillAppear(animated)
        //新規スタンプ画像フラグがtrueの場合、実行する処理
        if appDelegate.isNewStampAdded == true{
            //stampArrayの最後に入っている要素を取得
            let stamp = appDelegate.stampArray.last!
            //スタンプのフレームを設定
            stamp.frame = CGRectMake(0, 0, 100, 100)
            //スタンプの設置座標を写真画像の中心に設定
            stamp.center = mainImageView.center
            //スタンプのタッチ操作を許可
            stamp.userInteractionEnabled = true
            //スタンプを自分で配置したViewに設置
            canvasView.addSubview(stamp)
            //新規スタンプ画像フラグをfalseに設定
            appDelegate.isNewStampAdded = false
        }
    }
    
    
    
    /*
    通信終了時に呼び出されるデリゲート.
    */
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        print("didCompleteWithError")
        // エラーが有る場合にはエラーのコードを取得.
        print(error?.code)
    }
    
    func URLSession(session: NSURLSession,
        task: NSURLSessionTask,
        didReceiveChallenge challenge: NSURLAuthenticationChallenge,
        completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential?)
        -> Void) {
            
            print("willPerformHTTPRedirection")
            
    }
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        
        print("didSendBodyData")
        
        
    }
    
    /*
    通信が終了したときに呼び出されるデリゲート.
    */
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveData data: NSData) {
        
        // 帰ってきたデータを文字列に変換.
        var myData:NSString = NSString(data: data, encoding: NSUTF8StringEncoding)!
        
        // バックグラウンドだとUIの処理が出来ないので、メインスレッドでUIの処理を行わせる.
        dispatch_async(dispatch_get_main_queue(), {
            print(myData as String)
        })
        
    }
    
    func URLSessionDidFinishEventsForBackgroundURLSession(session: NSURLSession) {
        print("URLSessionDidFinishEventsForBackgroundURLSession")
        
        // バックグラウンドからフォアグラウンドの復帰時に呼び出されるデリゲート.
    }

    
    
    //画像をレンダリングして保存
    @IBAction func saveTapped(){
        //画像コンテキストをサイズ、透過の有無、スケールを指定して作成
        UIGraphicsBeginImageContextWithOptions(canvasView.bounds.size, canvasView.opaque, 0.0)
        //canvasViewのレイヤーをレンダリング
        canvasView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        //レンダリングした画像を取得
        let image = UIGraphicsGetImageFromCurrentImageContext()
        //画像コンテキストを破棄
        UIGraphicsEndImageContext()
        //取得した画像をフォトライブラリへ保存
        UIImageWriteToSavedPhotosAlbum(image, self,"image:didFinishSavingWithError:contextInfo:", nil)
    }
    
    
    //写真の保存後に呼ばれるメソッド
    
    func image(image: UIImage, didFinishSavingWithError error: NSError!, contextInfo: UnsafeMutablePointer<Void>) {
        let alert = UIAlertView()
        alert.title = "保存"
        alert.message = "保存完了です。"
        alert.addButtonWithTitle("OK")
        alert.show()
    }
    
    
    //スタンプ選択画面遷移メソッド
    
    @IBAction func stampTapped(){
        //SegueのIdentifierを設定
        self.performSegueWithIdentifier("ToMyStampList", sender: self)
    }
    
    
    //スタンプ画像の削除
    
    @IBAction func deleteTapped(){
        
        //canvasViewのサブビューの数が1より大きかったら実行
        
        if canvasView.subviews.count > 1{
            
            //canvasViewの子ビューの最後のものを取り出す
            
            let lastStamp = canvasView.subviews.last! as! Stamp
            
            //canvasViewからlastStampを削除する
            
            lastStamp.removeFromSuperview()
            
            //lastStampが格納されているstampArrayのインデックス番号を取得
            
            if let index = appDelegate.stampArray.indexOf(lastStamp){
                
                //stampArrayからlastStampを削除
                
                appDelegate.stampArray.removeAtIndex(index)
                
            }
        }
    }
    
    
    //UIImagePickerController画像取得メソッド
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage
        image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
            //引数imageに格納された画像をmainImageViewにセット
            mainImageView.image = image
            //カメラ画面もしくはフォトライブラリ画面を閉じる
            self.dismissViewControllerAnimated(true, completion: nil)
            
            self.send_Img()
    }
    
    
    //アクションシート表示メソッド
    @IBAction func cameraTapped(){
        //ボタンタイトルをNSLocalizedStringに変更
        let cancelString = NSLocalizedString("Cancel",comment:"キャンセル")
        let cameraString = NSLocalizedString("Camera",comment:"カメラ")
        let libraryString = NSLocalizedString("Library",comment:"ライブラリ")
        
        // UIActionSheet生成
        let actionSheet:UIAlertController = UIAlertController(title:"写真を取得",
            message: "写真の取得先を選択してください",
            preferredStyle: UIAlertControllerStyle.ActionSheet)
        // Cancelボタン
        let cancelAction:UIAlertAction = UIAlertAction(title: cancelString,
            style: UIAlertActionStyle.Cancel,
            handler:{
                (action:UIAlertAction!) -> Void in
                print("Cancel")
                
        })
        // Cameraボタン
        let cameraAction:UIAlertAction = UIAlertAction(title: cameraString,
            style: UIAlertActionStyle.Default,
            handler:{
                (action:UIAlertAction!) -> Void in
                print("Camera")
                //1番目のボタンを押したらソースタイプをカメラに設定
                self.pickerController.sourceType = UIImagePickerControllerSourceType.Camera
                //UIImagePickerControllerを表示
                self.presentViewController(self.pickerController, animated: true, completion: nil)
        })
        // Libraryボタン
        let libraryAction:UIAlertAction = UIAlertAction(title: libraryString,
            style: UIAlertActionStyle.Default,
            handler:{
                (action:UIAlertAction!) -> Void in
                print("Library")
                //2番目のボタンを押したらソースタイプをフォトライブラリに設定
                self.pickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                //UIImagePickerControllerを表示
                self.presentViewController(self.pickerController, animated: true, completion: nil)
        })
        // AlertViewControllerにボタンを追加
        actionSheet.addAction(cancelAction)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(libraryAction)
        // 画面表示
        presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


//
//  PostComment.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/26.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit

class PostComment: NSObject {
    var myRequest:NSMutableURLRequest!
    var myData:NSString!
    var mySession:NSURLSession!
    var json:NSData!

    
     func postRequest(comment:String){
        let myDict:NSMutableDictionary = NSMutableDictionary()
        myDict.setObject(comment, forKey: "comment")
        
        // 作成したdictionaryがJSONに変換可能かチェック.
        if NSJSONSerialization.isValidJSONObject(myDict){
            
            // DictionaryからJSON(NSData)へ変換.
            do {
                let json =  try NSJSONSerialization.dataWithJSONObject(myDict, options: NSJSONWritingOptions.PrettyPrinted)
                // 生成したJSONデータの確認.
                print("json完成")
                print(NSString(data: json, encoding: NSUTF8StringEncoding)!)
            } catch let error {
                // エラーが起こったらここに来るのでエラー処理などをする
                print("Json変換失敗")
                print(error)

            }
        }
        
        // Http通信のリクエスト生成.
        let myCofig:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        let myUrl:NSURL = NSURL(string: "\(Env.DocumentRoot())/add_comment")!
        
        self.myRequest = NSMutableURLRequest(URL: myUrl)
        
        self.mySession = NSURLSession(configuration: myCofig)
        print(self.mySession)
        
        myRequest.HTTPMethod = "POST"
        
        //        myRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        // jsonのデータを一度文字列にして、キーと合わせる.

        self.myData = "json=\(NSString(data: json, encoding: NSUTF8StringEncoding)!)"
        
        // jsonデータのセット.
        myRequest.HTTPBody = self.myData.dataUsingEncoding(NSUTF8StringEncoding)
        
    }
}

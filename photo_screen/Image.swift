//
//  Image.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/25.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit

class Image: NSObject,NSURLSessionDelegate,NSURLSessionDataDelegate {
    
    let name: String
    let title: String
    let password: String
    let imgview: UIImage
    
    let myDict:NSMutableDictionary = NSMutableDictionary()
    var myRequest:NSMutableURLRequest!
    var myData:NSString!
    var mySession:NSURLSession!
    var json:NSData!
    
    init(title: String ,name:String, password: String, photo:UIImage) {
        self.title = title
        self.name = name
        self.password = password
        self.imgview = photo
        
    }
    
    func postReqest(){
        
        
        
        myDict.setObject("object1", forKey: "image")
        myDict.setObject("object2", forKey: "name")
        myDict.setObject("object3", forKey: "image_url")
        myDict.setObject("object4", forKey: "title")
        
        // 作成したdictionaryがJSONに変換可能かチェック.
        if NSJSONSerialization.isValidJSONObject(myDict){
            
            // DictionaryからJSON(NSData)へ変換.
            do {
                let json =  try NSJSONSerialization.dataWithJSONObject(myDict, options: NSJSONWritingOptions.PrettyPrinted)
                self.json = json
                // 生成したJSONデータの確認.
                print(NSString(data: json, encoding: NSUTF8StringEncoding)!)
                
            } catch let error {
                // エラーが起こったらここに来るのでエラー処理などをする
                print("Json変換失敗")
                print(error)
            }
            
        }
        
        // Http通信のリクエスト生成.
        let myCofig:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        let myUrl:NSURL = NSURL(string: "http://localhost:3000/images")!
        
        self.myRequest = NSMutableURLRequest(URL: myUrl)
        
        self.mySession = NSURLSession(configuration: myCofig)
        print(self.mySession)
        
        myRequest.HTTPMethod = "POST"
        
        //        myRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        // jsonのデータを一度文字列にして、キーと合わせる.
        self.myData = "json=\(NSString(data: json, encoding: NSUTF8StringEncoding)!)"
        
        // jsonデータのセット.
        myRequest.HTTPBody = myData.dataUsingEncoding(NSUTF8StringEncoding)
        
    }
}
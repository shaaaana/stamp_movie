//
//  WebSampleViewController.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/24.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit

class WebSampleViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet var webview : UIWebView? = nil
    private var myTextField: UITextField!
    
    var targetURL = "http://localhost:3000/webview/1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // UITextFieldを作成する.
        myTextField = UITextField(frame: CGRectMake(0,0,200,30))
        
        // 表示する文字を代入する.
        myTextField.text = "Hello Swift!!"
        
        // Delegateを設定する.
        myTextField.delegate = self
        
        // 枠を表示する.
        myTextField.borderStyle = UITextBorderStyle.RoundedRect
        
        // UITextFieldの表示する位置を設定する.
        myTextField.layer.position = CGPoint(x:self.view.bounds.width/2,y:400);
        
        // Viewに追加する.
        self.view.addSubview(myTextField)
        loadAddressURL()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadAddressURL() {
        let requestURL = NSURL(string: targetURL)
        let req = NSURLRequest(URL: requestURL!)
        webview!.loadRequest(req)
    }
}
//
//  JsonGetViewController.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/24.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit

class JsonGetViewController: UIViewController,NSURLSessionDelegate,NSURLSessionDataDelegate{
    
    var myTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.lightGrayColor()
        
        //結果表示用のTextViewを用意
        myTextView = UITextView(frame: CGRectMake(10, 50, self.view.frame.width - 20, 500))
        
        myTextView.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 1, alpha: 1.0)
        myTextView.layer.masksToBounds = true
        myTextView.layer.cornerRadius = 20.0
        myTextView.layer.borderWidth = 1
        myTextView.layer.borderColor = UIColor.blackColor().CGColor
        myTextView.font = UIFont.systemFontOfSize(CGFloat(20))
        myTextView.textColor = UIColor.blackColor()
        myTextView.textAlignment = NSTextAlignment.Left
        myTextView.dataDetectorTypes = UIDataDetectorTypes.All
        myTextView.layer.shadowOpacity = 0.5
        myTextView.layer.masksToBounds = false
        myTextView.editable = false
        
        self.view.addSubview(myTextView)
        
        // 通信用のConfigを生成.
        let myConfig:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        // Sessionを生成.
        let mySession:NSURLSession = NSURLSession(configuration: myConfig, delegate: self, delegateQueue: nil)
        
        // 通信先のURLを生成.
        let myUrl:NSURL = NSURL(string: "http://localhost:3000/images.json")!
        
        // タスクの生成.
        let myTask:NSURLSessionDataTask = mySession.dataTaskWithURL(myUrl, completionHandler: { (data, response, err) -> Void in
            
            // 受け取ったjsonデータを表示.
            dispatch_async(dispatch_get_main_queue(), {
                
                self.myTextView.text = NSString(data: data!, encoding: NSUTF8StringEncoding)! as String
            })
            
            // 受け取ったJSONデータをパースする.
            do{
                var json:NSArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSArray
                
                // バックグラウンドだとUIの処理が出来ないので、メインスレッドでUIの処理を行わせる.
                dispatch_async(dispatch_get_main_queue(), {
                    
                    // パースしたJSONデータへのアクセス.
                    for dic in json as! [NSDictionary]{
                        for value in dic{
                            self.myTextView.text = "\(self.myTextView.text)\n\(value.key) : \(value.value)"
                        }
                    }
                })
            }catch{
                print(error)
            }
        })
        
        // タスクの実行.
        myTask.resume()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}

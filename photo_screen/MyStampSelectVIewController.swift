//
//  StampSelectViewController.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/18.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit

class MyStampSelectViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate {
    //画像を格納する配列
    var imageArray:[UIImage] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        var arr = ["dl2.png","18.png","20.png","15.png"];
        //配列imageArrayに1〜6.pngの画像データを格納
        for i in arr{
            print("i:\(i)")
            let url = NSURL(string: "http://localhost:8888/Images/\(i)");
            var err: NSError?;
            let req = NSURLRequest(URL:url!)
            NSURLConnection.sendAsynchronousRequest(req, queue:NSOperationQueue.mainQueue()){(res, data, err) in
                let image = UIImage(data:data!)
                print("image:\(image)")
                // 画像に対する処理 (UcellのUIImageViewに表示する等)
                self.imageArray.append(image!)
            }
            
//            var img = UIImage(data:imageData);
            
        }
    }
    
    
    //コレクションビューのセルが選択された時のメソッド
    func collectionView(collectionView: UICollectionView,didSelectItemAtIndexPath indexPath: NSIndexPath) {
        //Stampインスタンスを作成
        let stamp = MyStamp()
        //stampにインデックスパスからスタンプ画像を設定
        stamp.image = self.imageArray[indexPath.row]
        //AppDelegateのインスタンスを取得
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        //配列stampArrayにstampを追加
        appDelegate.MystampArray.append(stamp)
        //新規スタンプ追加フラグをtrueに設定
        appDelegate.isNewMyStampAdded = true
        //スタンプ選択画面を閉じる
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    //スタンプ選択画面を閉じるメソッド
    @IBAction func closeTapped(){
        //モーダルで表示した画面を閉じる
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //戻り値にimageArrayの要素数を設定
        return imageArray.count
    }
    
    
    //コレクションビューのセルを設定
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath)-> UICollectionViewCell {
        //UICollectionViewCellを使うための変数を作成
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        //セルのなかの画像を表示するImageViewのタグを指定
        let imageView = cell.viewWithTag(1) as! UIImageView
        //セルの中のImage Viewに配列の中の画像データを表示
        imageView.image = self.imageArray[indexPath.row]
        print("imageView.image:\(imageView.image)")
        //設定したセルを戻り値にする
        return cell
    }
    
    //スタンプ選択画面遷移メソッド
    
    @IBAction func nextTapped(){
        //SegueのIdentifierを設定
        self.performSegueWithIdentifier("ToMyStampList", sender: self)
    }
}

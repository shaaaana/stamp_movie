//
//  PostImageViewController.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/26.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit

class PostImageViewController: UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate{
    
    //    private var titleTextField: UITextField!
    //    private var nameTextField: UITextField!
    //    private var passwordTextField: UITextField!
    //    enum TextFieldType {
    //        case titleTextField
    //        case nameTextField
    //        case passwordTextField
    //    }
    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    
    @IBOutlet var myTextField: UITextField!
    //    myTextField.tag = TextFieldType.titleTextField.hashValue
    
    
    var pickerController = UIImagePickerController()
    var json:NSData!
    var myTextView: UITextView!
    var photo: UIImageView!
    @IBOutlet var canvasView: UIView!
    var imgPostManager: ImageManager!
    //UIImagePickerControllerで取得した画像を表示
    var name:String = ""
    var phototitle:String = ""
    var password:String = ""
    
    var myimage:Image!
    
    
    @IBOutlet var mainImageView:UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //UIImagePickerControllerのデリゲートメソッドを使用する設定
        pickerController.delegate = self
        
        self.titleTextField.delegate = self
        self.nameTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.view.backgroundColor = UIColor.lightGrayColor()
        
        // 結果表示用のTextViewを用意.
        myTextView = UITextView(frame: CGRectMake(10, 50, self.view.frame.width - 20, 100))
        
        myTextView.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 1, alpha: 1.0)
        myTextView.layer.masksToBounds = true
        myTextView.layer.cornerRadius = 20.0
        myTextView.layer.borderWidth = 1
        myTextView.layer.borderColor = UIColor.blackColor().CGColor
        myTextView.font = UIFont.systemFontOfSize(CGFloat(20))
        myTextView.textColor = UIColor.blackColor()
        myTextView.textAlignment = NSTextAlignment.Left
        myTextView.dataDetectorTypes = UIDataDetectorTypes.All
        myTextView.layer.shadowOpacity = 0.5
        myTextView.layer.masksToBounds = false
        myTextView.editable = false
        
        //        self.view.addSubview(myTextView)
    }
    
    
    @IBAction func topost(){
        
        
        print("title",titleTextField.text!)
        print("name",nameTextField.text!)
        print("pass",passwordTextField.text!)
        //        print("photo",self.photo)
        if titleTextField.text == "" || nameTextField.text == "" || passwordTextField.text == ""{
            displayAlert("入力エラー", message: "記入欄に空欄があります。")
            return
        }
        
        if(self.photo == nil){
            displayAlert("入力エラー", message: "写真を投稿してください")
            return
        }
        
        print(self.photo.image)
        
        var myimage = Image(title: titleTextField.text!, name: nameTextField.text!, password: passwordTextField.text!, photo: mainImageView.image!)
        
        
        //        if(validated(image)){
        save()
        //        }
        
    }
    //
    
    
    //バリデーション発動時に表示するアラート
    func displayAlert(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    
    func save(){
        // dictionaryで送信するJSONデータを生成.
        myimage.postReqest()
        
        let myTask:NSURLSessionDataTask = imgPostManager.mySession.dataTaskWithRequest(imgPostManager.myRequest, completionHandler: { (data, response, err) -> Void in
            
            // バックグラウンドだとUIの処理が出来ないので、メインスレッドでUIの処理を行わせる.
            dispatch_async(dispatch_get_main_queue(), {
                //                self.myTextView.text = NSString(data: data!, encoding: NSUTF8StringEncoding)! as String
            })
        })
        
        myTask.resume()
    }
    
    
    //UIImagePickerController画像取得メソッド
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage
        image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
            //引数imageに格納された画像をmainImageViewにセット
            mainImageView.image = image
            self.photo = mainImageView
            //カメラ画面もしくはフォトライブラリ画面を閉じる
            self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func cameraTapped(){
        //ボタンタイトルをNSLocalizedStringに変更
        let cancelString = NSLocalizedString("Cancel",comment:"キャンセル")
        let cameraString = NSLocalizedString("Camera",comment:"カメラ")
        let libraryString = NSLocalizedString("Library",comment:"ライブラリ")
        
        // UIActionSheet生成
        let actionSheet:UIAlertController = UIAlertController(title:"写真を取得",
            message: "写真の取得先を選択してください",
            preferredStyle: UIAlertControllerStyle.ActionSheet)
        // Cancelボタン
        let cancelAction:UIAlertAction = UIAlertAction(title: cancelString,
            style: UIAlertActionStyle.Cancel,
            handler:{
                (action:UIAlertAction!) -> Void in
                print("Cancel")
                
        })
        // Cameraボタン
        let cameraAction:UIAlertAction = UIAlertAction(title: cameraString,
            style: UIAlertActionStyle.Default,
            handler:{
                (action:UIAlertAction!) -> Void in
                print("Camera")
                //1番目のボタンを押したらソースタイプをカメラに設定
                self.pickerController.sourceType = UIImagePickerControllerSourceType.Camera
                //UIImagePickerControllerを表示
                self.presentViewController(self.pickerController, animated: true, completion: nil)
        })
        // Libraryボタン
        let libraryAction:UIAlertAction = UIAlertAction(title: libraryString,
            style: UIAlertActionStyle.Default,
            handler:{
                (action:UIAlertAction!) -> Void in
                print("Library")
                //2番目のボタンを押したらソースタイプをフォトライブラリに設定
                self.pickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                //UIImagePickerControllerを表示
                self.presentViewController(self.pickerController, animated: true, completion: nil)
        })
        // AlertViewControllerにボタンを追加
        actionSheet.addAction(cancelAction)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(libraryAction)
        // 画面表示
        presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    /*
    UITextFieldが編集された直後に呼ばれるデリゲートメソッド.
    */
    func textFieldDidBeginEditing(textField: UITextField){
        print(textField)
        
    }
    
    /*
    UITextFieldが編集終了する直前に呼ばれるデリゲートメソッド.
    */
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        print("textFieldShouldEndEditing:" + textField.text!)
        return true
    }
    //
    /*
    改行ボタンが押された際に呼ばれるデリゲートメソッド.
    */
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        //        print(textField)
        return true
    }
    //
    //    func set_val(tag:Int, myTextField:UITextField){
    //        var textstr = myTextField.text! as String
    //        myTextField.delegate = self;
    //        switch tag{
    //        case 1:
    //            //処理１
    //            self.phototitle = textstr
    //            print(self.phototitle)
    //            break;
    //        case 2:
    //            print(TextFieldType.nameTextField.hashValue)
    //            //処理2
    //            self.name = textstr
    //            print(self.name)
    //            break;
    //        case 3:
    //            //処理3:
    //            print(TextFieldType.passwordTextField.hashValue)
    //            self.password = textstr
    //            print(self.password)
    //            break;
    //        default:
    //            // 上記以外
    //            break;
    //        }
    //    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}
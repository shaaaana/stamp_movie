//
//  ViewController.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/17.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    var pickerController = UIImagePickerController()
    //スタンプ画像を配置するUIView
    @IBOutlet var canvasView: UIView!
    //AppDelegateを使うための変数
    var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    //UIImagePickerControllerで取得した画像を表示
    @IBOutlet var mainImageView:UIImageView!
    
    //9.描画エリアを接続
    @IBOutlet weak var drawViewArea: DrawView!
    
    enum ColorBtnTag: Int {
        case Zero = 1, Red, Black, Blue
        static let allValues: [ColorBtnTag] = [ Red, Black, Blue]
        
        func Color() -> String {
            return String(self )
        }
        
        func set_color(btn: UIButton)->Void{
            switch self{
            case .Red:
                btn.backgroundColor = UIColor.redColor()
            case .Black:
                btn.backgroundColor = UIColor.blackColor()
            case .Blue:
                btn.backgroundColor = UIColor.blueColor()
            default: break
            }
        }
    }
        
    enum WidthBtnTag: Int {
        case Zero = 1, Ten, Five, Three
        static let allValues: [WidthBtnTag] = [ Ten, Five, Three]
        
        func set_width(btn :UIButton)->Void{
            switch self{
            case .Ten:
                btn.backgroundColor = UIColor.whiteColor()
            case .Five:
                btn.backgroundColor = UIColor.whiteColor()
            case .Three:
                btn.backgroundColor = UIColor.whiteColor()
            default: break
            }
        }
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //UIImagePickerControllerのデリゲートメソッドを使用する設定
        pickerController.delegate = self
        set_colorbtn()
    }
    
    
    
   func set_colorbtn(){
        for btag in ColorBtnTag.allValues {
        var x = CGFloat(35 * btag.rawValue - 1)
        var btn = UIButton(frame: CGRectMake(x, 40, 30, 20))
        btn.tag = btag.rawValue
        switch(btag){
            case .Red:
                btn.backgroundColor = UIColor.redColor()
            case .Black:
                btn.backgroundColor = UIColor.blackColor()
            case .Blue:
                btn.backgroundColor = UIColor.blueColor()
            default: break
        }
    //            ColorBtnTag.set_color(btn)
    
    //            btn.setTitle(btag.Color(), forState: .Normal)
            btn.addTarget(self, action: "color_changed:", forControlEvents: .TouchUpInside)
            self.view.addSubview(btn)
        }
    }
    
    
    func color_changed(sender: UIButton){
        var btag = ColorBtnTag(rawValue: sender.tag)!
//        let theDrawView : DrawView = drawViewArea as DrawView
        if btag == .Blue {
            drawViewArea.r = 0
            drawViewArea.g = 0
            drawViewArea.b = 1
            drawViewArea.a = 1
            print("Blue Button color_changed!")
        }else if btag == .Red{
            drawViewArea.r = 1
            drawViewArea.g = 0
            drawViewArea.b = 0
            drawViewArea.a = 1
            print("Red Number is " + btag.Color())
        }else{
            print("Other Number is " + btag.Color())
            drawViewArea.r = 0
            drawViewArea.g = 0
            drawViewArea.b = 255
            drawViewArea.a = 1
        }
    }
    
    
    func set_widthbtn(){
        for btag in WidthBtnTag.allValues {
            var x = CGFloat(50 * btag.rawValue - 1)
            var btn = UIButton()
            btn.tag = btag.rawValue
            switch(btag){
            case .Ten:
                btn.frame = CGRectMake(x, 60, 30, 20)
            case .Five:
                btn.frame = CGRectMake(x, 60, 30, 10)
            case .Three:
                btn.frame = CGRectMake(x, 60, 30, 6)
            default: break
            }
            //            ColorBtnTag.set_color(btn)
            
            //            btn.setTitle(btag.Color(), forState: .Normal)
            btn.addTarget(self, action: "width_changed:", forControlEvents: .TouchUpInside)
            self.view.addSubview(btn)
        }
    }
    
    
    func width_changed(sender: UIButton){
        var btag = WidthBtnTag(rawValue: sender.tag)!
        //        let theDrawView : DrawView = drawViewArea as DrawView
        if btag == .Ten {
            drawViewArea.width = 10
//            print("Blue Button color_changed!")
        }else if btag == .Five{
            drawViewArea.width = 6
//            print("Red Number is " + btag.Color())
        }else{
//            print("Other Number is " + btag.Color())
            drawViewArea.width = 3
        }
    }
    
    
    
    //画面表示の直前に呼ばれるメソッド
    
    override func viewWillAppear(animated: Bool) {
        //viewWillAppearを上書きするときに必要な処理
        super.viewWillAppear(animated)
        //新規スタンプ画像フラグがtrueの場合、実行する処理
        if appDelegate.isNewStampAdded == true{
            //stampArrayの最後に入っている要素を取得
            let stamp = appDelegate.stampArray.last!
            //スタンプのフレームを設定
            stamp.frame = CGRectMake(0, 0, 100, 100)
            //スタンプの設置座標を写真画像の中心に設定
            stamp.center = mainImageView.center
            //スタンプのタッチ操作を許可
            stamp.userInteractionEnabled = true
            //スタンプを自分で配置したViewに設置
            canvasView.addSubview(stamp)
            //新規スタンプ画像フラグをfalseに設定
            appDelegate.isNewStampAdded = false
        }
    }
    
    
    //画像をレンダリングして保存
    @IBAction func saveTapped(){
        //画像コンテキストをサイズ、透過の有無、スケールを指定して作成
        UIGraphicsBeginImageContextWithOptions(canvasView.bounds.size, canvasView.opaque, 0.0)
        //canvasViewのレイヤーをレンダリング
        canvasView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        //レンダリングした画像を取得
        let image = UIGraphicsGetImageFromCurrentImageContext()
        //画像コンテキストを破棄
        UIGraphicsEndImageContext()
        //取得した画像をフォトライブラリへ保存
        UIImageWriteToSavedPhotosAlbum(image, self,"image:didFinishSavingWithError:contextInfo:", nil)
    }
    

    //写真の保存後に呼ばれるメソッド
    func image(image: UIImage, didFinishSavingWithError error: NSError!, contextInfo: UnsafeMutablePointer<Void>) {
        let alert = UIAlertView()
        alert.title = "保存"
        alert.message = "保存完了です。"
        alert.addButtonWithTitle("OK")
        alert.show()
    }
    
    
    //スタンプ選択画面遷移メソッド
    @IBAction func stampTapped(){
        //SegueのIdentifierを設定
        self.performSegueWithIdentifier("ToStampList", sender: self)
    }
    

    //スタンプ画像の削除
    @IBAction func deleteTapped(){
        //canvasViewのサブビューの数が1より大きかったら実行
        if canvasView.subviews.count > 1{
            //canvasViewの子ビューの最後のものを取り出す
            let lastStamp = canvasView.subviews.last! as! Stamp
            //canvasViewからlastStampを削除する
            lastStamp.removeFromSuperview()
            //lastStampが格納されているstampArrayのインデックス番号を取得
            if let index = appDelegate.stampArray.indexOf(lastStamp){
                
                //stampArrayからlastStampを削除
                
                appDelegate.stampArray.removeAtIndex(index)
                
            }
            
        }
        
    }
    
    
    @IBAction func claerTapped() {      //イラストの削除
        //10.
        let theDrawView : DrawView = drawViewArea as DrawView    //
        theDrawView.lines = []
        theDrawView.setNeedsDisplay()
    }
    
    
    //UIImagePickerController画像取得メソッド
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage
        image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
            //引数imageに格納された画像をmainImageViewにセット
            mainImageView.image = image
            //カメラ画面もしくはフォトライブラリ画面を閉じる
            self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    //アクションシート表示メソッド
    @IBAction func cameraTapped(){
        //ボタンタイトルをNSLocalizedStringに変更
        let cancelString = NSLocalizedString("Cancel",comment:"キャンセル")
        let cameraString = NSLocalizedString("Camera",comment:"カメラ")
        let libraryString = NSLocalizedString("Library",comment:"ライブラリ")
        
        // UIActionSheet生成
        let actionSheet:UIAlertController = UIAlertController(title:"写真を取得",
            message: "写真の取得先を選択してください",
            preferredStyle: UIAlertControllerStyle.ActionSheet)
        // Cancelボタン
        let cancelAction:UIAlertAction = UIAlertAction(title: cancelString,
            style: UIAlertActionStyle.Cancel,
            handler:{
                (action:UIAlertAction!) -> Void in
                print("Cancel")
                
        })
        // Cameraボタン
        let cameraAction:UIAlertAction = UIAlertAction(title: cameraString,
            style: UIAlertActionStyle.Default,
            handler:{
                (action:UIAlertAction!) -> Void in
                print("Camera")
                //1番目のボタンを押したらソースタイプをカメラに設定
                self.pickerController.sourceType = UIImagePickerControllerSourceType.Camera
                //UIImagePickerControllerを表示
                self.presentViewController(self.pickerController, animated: true, completion: nil)
        })
        // Libraryボタン
        let libraryAction:UIAlertAction = UIAlertAction(title: libraryString,
            style: UIAlertActionStyle.Default,
            handler:{
                (action:UIAlertAction!) -> Void in
                print("Library")
                //2番目のボタンを押したらソースタイプをフォトライブラリに設定
                self.pickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
                //UIImagePickerControllerを表示
                self.presentViewController(self.pickerController, animated: true, completion: nil)
        })
        // AlertViewControllerにボタンを追加
        actionSheet.addAction(cancelAction)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(libraryAction)
        // 画面表示
        presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


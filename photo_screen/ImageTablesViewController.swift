////
////  ImageTablesViewController.swift
////  photo_screen
////
////  Created by Ryosei Takahashi on 2016/06/25.
////  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
////
//
//import UIKit
//import Alamofire
//
//class ImageTablesViewController: UIViewController {
//    override func viewDidLoad() {
//        
//        super.viewDidLoad()
//        
//        let requestUrl = "https://ajax.googleapis.com/ajax/services/search/news?v=1.0&topic=p&hl=ja&rsz=8"
//        
//        //Webサーバに対してHTTP通信のリクエストを出してデータを取得
//        
//        Alamofire.request(.GET, requestUrl, parameters: ["foo": "bar"]).responseJSON { response in
//            
//            if let json = response.result.value {
//                
//                //まずJSONデータをNSDictionary型に
//                
//                let jsonDic = json as! NSDictionary
//                
//                //辞書化したjsonDicからキー値"responseData"を取り出す
//                
//                let responseData = jsonDic["responseData"] as! NSDictionary
//                
//                //responseDataからキー値"results"を取り出す
//                
//                self.newsDataArray = responseData["results"] as! NSArray
//                
//                print(self.newsDataArray)
//                
//            }
//        }
//    }
//}

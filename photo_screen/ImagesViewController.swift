//
//  ImagesViewController.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/24.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit

class ImagesViewController: UITableViewController,NSURLSessionDelegate,NSURLSessionDataDelegate{
    
    var myTextView: UITextView!
    
    // APIのURLを定義
    // APPID=XXXは先程取得したAPI KEYを各自設定してください
    var urlString = "http://localhost:3000/images.json"
    var cellItems = NSMutableArray()
    var cellNum = 0
    var selectedInfo : String?
//    var url_arr:[String] = []
    var detail_url: String = ""
    var url:String = ""
    var phototitle:String = ""
    var id:Int = 0
    
    
    // セクション数を設定
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    // 1セクションあたりの行数を設定
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(self.cellNum)
//        self.cellNum = cellItems.count
        
        print(self.cellNum )
        return self.cellNum
    }
    
    // コメントアウトされてるのをはずす & cellに表示をつっこむ
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        print(self.cellItems)
        print(self.cellItems.count)
        
        let cell = tableView.dequeueReusableCellWithIdentifier("ImageCell", forIndexPath: indexPath)
        if self.cellItems.count > 0 {
            print("items",cellItems)
            
            cell.textLabel?.text = self.cellItems[indexPath.row] as? String
//            cell.textLabel?.text = cell.viewWithTag(1)
            let titleLabel = cell.viewWithTag(1) as? UILabel
            titleLabel!.text = cellItems[indexPath.row]["title"] as? String
            let nameLabel = cell.viewWithTag(2) as! UILabel
            nameLabel.text = cellItems[indexPath.row]["name"] as? String
            
            let dataLabel = cell.viewWithTag(3) as! UILabel
            dataLabel.text = cellItems[indexPath.row]["updated_at"] as? String

//            画像表示(後で編集)
            let imageView = cell.viewWithTag(4) as! UIImageView
            let imgpath = cellItems[indexPath.row]["image_url"] as? String
            let Image_Url = "\(Env.DocumentRoot())\(imgpath!)"
            print("Image_Url",Image_Url)
            let url = NSURL(string: Image_Url);
            var err: NSError?;
            let req = NSURLRequest(URL:url!)
            NSURLConnection.sendAsynchronousRequest(req, queue:NSOperationQueue.mainQueue()){(res, data, err) in
//                let image = UIImage(data:data!)

                imageView.image = UIImage(data:data!)
            }
        }
        return cell
    }
    // 継承時は書かれていない。メソッドを追加。
    // テーブルのcellを選択した時に呼ばれる関数。
    // その中で先ほど作成したsegueを呼び出して画面遷移させる
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.dequeueReusableCellWithIdentifier("ImageCell", forIndexPath: indexPath)
//        self.selectedInfo = self.cellItems[indexPath.row] as! String
//        self.detail_url = self.url_arr[indexPath.row]
        let documentroot = Env.DocumentRoot()
        self.id = self.cellItems[indexPath.row]["id"] as! Int
//        print(id)
        let detail_url = "\(documentroot)/webview/\(self.id)"
        self.url = detail_url
        print("title",self.cellItems[indexPath.row]["title"])
        if((self.cellItems[indexPath.row]["title"] == nil)){
            phototitle = "未設定"
            performSegueWithIdentifier("toDetail", sender: nil)
            return
        }
        let str = self.cellItems[indexPath.row]["title"] as! String
        self.phototitle = str
//        print("detail:",detail_url)
        performSegueWithIdentifier("toDetail", sender: nil)
    }
    
    // segueで遷移するときに、行われる前処理
    // 今選択されたcellの情報を遷移先の画面に渡す
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "toDetail") {
            let sVC = segue.destinationViewController as! DetailViewController
//            viewController.info = self.selectedInfo
            sVC.view_url = self.url
            sVC.phototitle = self.phototitle
            sVC.id = self.id
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeTableData()
        self.view.backgroundColor = UIColor.lightGrayColor()
//        makeTableData()
    }
    

    
    
    func makeTableData() {
//        let image = Image()
        
        // 非同期でAPIを叩いている
        // 通信用のConfigを生成.
        let myConfig:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        // Sessionを生成.
        let mySession:NSURLSession = NSURLSession(configuration: myConfig, delegate: self, delegateQueue: nil)
        
        // 通信先のURLを生成.
        let myUrl:NSURL = NSURL(string: "http://localhost:3000/images.json")!
        
        // タスクの生成.
        let myTask:NSURLSessionDataTask = mySession.dataTaskWithURL(myUrl, completionHandler: { (data, response, err) -> Void in
            
            // 受け取ったjsonデータを表示.
            dispatch_async(dispatch_get_main_queue(), {
//                                self.myTextView.text = NSString(data: data!, encoding: NSUTF8StringEncoding)! as String
//                print(NSString(data: data!, encoding: NSUTF8StringEncoding)! as String)
            })
            
            // 受け取ったJSONデータをパースする.
            do{
                let jsonarr:NSArray = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSArray
                
                // バックグラウンドだとUIの処理が出来ないので、メインスレッドでUIの処理を行わせる.
                dispatch_async(dispatch_get_main_queue(), {
                    // パースしたJSONデータへのアクセス.
                    for dic in jsonarr as! [NSDictionary]{
                        self.cellItems.addObject(dic)
                    }
                    self.cellNum = self.cellItems.count
                    print(self.cellNum)
                    self.tableView.reloadData()
                })
                
                
            }catch{
                print(error)
                print("error")
            }
        })
        // タスクの実行.
        myTask.resume()
        //        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}

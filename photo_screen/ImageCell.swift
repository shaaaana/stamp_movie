//
//  ImageCell.swift
//  photo_screen
//
//  Created by Ryosei Takahashi on 2016/06/25.
//  Copyright © 2016年 Ryosei Takahashi. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var lectureImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var created: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
